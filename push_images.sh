#!/bin/bash
shopt -s globstar
set -e

for dockerfile in **/Dockerfile; do
    tagPath=$(dirname "$dockerfile")
    imagePath=$(dirname "$tagPath")
    tag=$(basename "$tagPath")
    image=$(basename "$imagePath")
    imageName="registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/${image}:${tag}"

    echo
    echo "Pushing $imageName"
    docker push "$imageName"
done
