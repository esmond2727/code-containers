#!/bin/bash
set -e
shopt -s globstar

# We shall do this instead as due to the lack of space on the ci instance, we cannot build everything THEN test sadly

# Required args
foldertogo=$1

# Optional argument to test single image
# i.e. aasp/python:latest
imageToTest=$2

runTest() {
    imageName=$1
    payloadFile=$2
    resultFile=$3
    testName="$imageName - $(basename "$(dirname "$payloadFile")")"

    result=$(docker run -i --rm "$imageName" < "$payloadFile")
    expect=$(cat "$resultFile")

    if [ "$result" == "$expect" ]; then
        echo "OK: $testName"
    else
        echo "FAILED: $testName"
        echo "Result: $result"
        exit 1
    fi
}

echo "Building Language $foldertogo"
cd "$foldertogo"

for dockerfilePath in **/Dockerfile; do
    (
        tag=$(dirname "$dockerfilePath")
        imagePath=$(basename "$PWD")
        image=$(basename "$imagePath")
        imageName="registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/${image}:${tag}"

        # Change directory to tag path
        cd "$tag"

        # Build image (x64 only so we do not require multi-arch here)
        echo "Building $imageName"
        docker build --pull --no-cache --network host -t "$imageName" --build-arg downloadurl=$x64_BINARY .

        # Ensure that the tests directory exist
        mkdir -p tests

        # Find and run tests
        for payloadFile in tests/**/payload.json; do
            testPath=$(dirname "$payloadFile")
            resultFile="${testPath}/result.json"

            # If imageToTest is set; run only tests for that image – run all tests if not
            if [ -z "$imageToTest" ] || [ "$imageToTest" == "$imageName" ] ;then
                runTest "$imageName" "$payloadFile" "$resultFile"
            fi
        done
    )
done
